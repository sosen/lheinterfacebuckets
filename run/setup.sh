# Have to do this before setting up the special python version
# or it doesn't work. Not sure why....
export PYTHONPATH="${PYTHONPATH}:$PWD/../../batch_management/"
# Now we can do this
source /cvmfs/sft.cern.ch/lcg/views/LCG_96python3/x86_64-slc6-gcc8-opt/setup.sh
lsetup "root 6.18.00-x86_64-slc6-gcc8-opt"
# Our build...
if [ -f ../build/setup.sh ]; then
    echo "Calling build/setup.sh"
    source ../build/setup.sh
else
    echo "No build/setup.sh"
fi
