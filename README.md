Histogram making code for BSM 4-tops
====================================

This code runs on post-production ntuples to make histograms. It is based on RDataFrame and takes inspiration from the [di-Higgs to 4b analysis](https://gitlab.cern.ch/hh4b/hh4b-resolved-reconstruction/blob/master/README.md).

You DON'T want to have an ATLAS release set up to use this. It just uses a standalone ROOT version, and it needs 6.18 which is more recent than anything we currently have in the releases. So run this from a clean shell.

Now, to set up and compile:
```
cd run
source setup.sh
```
(you'll get a "No build/setup.sh" error this once)

```
cd ..
mkdir build
cd build
cmake ..
make
```

There should be a `build/setup.sh` file now: source it now. This will be automatically called each time you log into a new shell when you source the setup.sh script in the run directory.

Note this is equivalent to `build/x86_64-slc6-gcc62-opt/setup.sh` in an ATLAS package, but unlike in an ATLAS package, you do *not* have to source it again every time you compile. It just points to the location of your executables, so you automatically access the updated ones after compiling without needing to call this again.

Running the code
----------------

Histogram running is controlled by `run/run_kinematic_hists.py`. Similarly to the post-production code, this will run on the batch with one job per input/output root file. One can provide several arguments to this script:
 - `useBatch` and `batch_type`: Self-evident. Supported batch types are condor and pbs.
 - `tag`: Run on these post-production histograms
 - `tag_out`: If this is filled, outputs will have this name. Otherwise, will be named with `tag`.
 - `ntup_path`: Where your post-processing ntuples to run on are.
 - `data_dirs`: Which subprocesses to use. It's a misnomer - signal and MC are included here too.
 - `selections`: Which of the BSM 4-tops selections to run on
 - `matching`: Basically a wildcard if you want to run only on a subset of input files. For example, put `ttbar` to run only ttbar MC.
 - `test`: If this is set to True, no jobs will be submitted, but the commands will be printed out.
 - `trees`: Comma-separated list of the tree names to consider.
 - `output_dir`: The directory in which the output root files will be stored.
 - `email`: The email address to use for sending emails about job status. 

One job will be submitted per input file. They don't take very long - maximum is about half an hour - but there are a lot so it's good to do them like this. An example command line can be found below:

```
python run_kinematic_hists.py --selections=BSM_ejets_DL1,BSM_mujets_DL1 --data_dirs=mc16a --tag_out=test --matching=Wjets --test=True --output_dir=/nfs/dust/atlas/user/lvalery/4tops/plotting/
```

Making changes in the code
--------------------------

RDataFrame is lazy-evaluated, so it won't do anything until it has to. This means it is very much in your interest to keep anything which forces evaluation (`histwrapper->GetValue()`, `histwrapper->Integral()`, etc) at the very end. 

Currently nothing is evaluated until after all regions have been calculated and the output file is being written (`hist.GetValue().Write()`). This means that the entire code takes only as long as one `TTree->Draw()`. You can add as many additional complications to the region defitinitions and as many extra output histograms as you like without increasing the time taken by the code. 

However, if you add anything in the region definition for loops or otherwise earlier in the code which forces evaluation, you will see that the execution time will grow dramatically. Please avoid doing that.
